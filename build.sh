#!/bin/sh

if [ -z $DOCKER_RUNNING ]; then
    echo "This script needs to be run in a Docker container."
    exit 1
else
    build_dir=/workspaces/lib/build

    mkdir -p $build_dir && cd $build_dir

    # if build script was executed before, this clears the CMake cache
    rm -rf $build_dir/*

    pwd
    cmake ..
    cmake --build .

    # DEB can also be i.e. RPM when building for RPM-based distros.
    # Please refer to the correct documentation for your generator.
    # https://cmake.org/cmake/help/latest/manual/cpack-generators.7.html
    cpack -G DEB
fi