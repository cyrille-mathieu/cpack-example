# Packaging with CPack Example

This project was created as an example on how to create an OS-level package for Howest.

All the required files are included, building a .deb package can be done by executing the `build.sh` script. 

The source code used in this project comes from https://github.com/retifrav/cmake-cpack-example.

**Note:** It is assumed that this project is developed in a VSCode Dev Container! The necessary files for this development environment are also included in the `.devcontainer` directory.
